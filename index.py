from datetime import datetime
from elasticsearch import Elasticsearch



# es = Elasticsearch()
# doc = {
#    'author': 'Ethan',
#    'text': 'Starway to the foundation',
#    'timestamp': datetime.now(),
#}


class TDDClass():

    def __init__(self):
        self.index = 'tdd-index'
        self.es = Elasticsearch(host="localhost", port=9200)

    def insert(self, doc):
        res = self.es.index(index="tdd-index", id=1, body=doc)

    def search(self, key, value):
        res = self.es.search(index='tdd-index', body={
            "query": {
                "match": {
                    key: value
                }
            }
        })
        print(res)

    def delete(self, key):
        self.es.delete(self.index, body={
            "query": {
                "match": {
                    key: value
                }
            }
        })


tdd = TDDClass()
doc = {
    'author': 'Ethan',
    'text': 'Starway to the foundation',
    'timestamp': datetime.now(),
}

tdd.insert(doc)

# tdd.search("author", "Ethan")
